<?php
	
	if (!isset($_POST['token'])){
		die( "Token not sent!" );
		exit;
	} 

	session_id($_POST['token']);
    session_start();

	if($_SESSION['token'] !== $_POST['token']){
		die("Request forgery detected");
	}

?>
<!DOCTYPE html>
<html>
<head><title>Module 5 - JS Calculator </title>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="calendar.css" />
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="month_naviator">
	<div class="col-md-1">
		<button type="button" id="prev_m" class="btn btn-default btn-lg"><-</button>
	</div>
	<div class="col-md-1"></div>
	<div class="col-md-1"></div>
	<div class="col-md-1"></div>
	<div class="col-md-1"></div>
	<div class="col-md-1"></div>
	<div class="col-md-1">
		<button type="button" id="next_m" class="btn btn-default btn-lg">-></button>
	</div>
</div>
<br>
<br>
<br>
<div class="month">
  <div id="month_text" class="col-md-7 text-center"> Month</div>
</div>
<br>
<div class="row">
  <div class="col-md-1 text-center">Monday</div>
  <div class="col-md-1 text-center">Tuesday</div>
  <div class="col-md-1 text-center">Wednesday</div>
  <div class="col-md-1 text-center">Thursday</div>
  <div class="col-md-1 text-center">Friday</div>
  <div class="col-md-1 text-center">Saturday</div>
  <div class="col-md-1 text-center">Sunday</div>
</div>
<br>
<div class="calendar_1_week">
	<div class="col-md-1">
		<button type="button" id="c_1_1" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_1_2" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_1_3" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_1_4" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_1_5" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_1_6" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_1_7" class="btn btn-default btn-lg"></button>
	</div>
	<br>

</div>
<br>
<div class="calendar_2_week">
	<div class="col-md-1">
		<button type="button" id="c_2_1" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_2_2" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_2_3" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_2_4" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_2_5" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_2_6" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_2_7" class="btn btn-default btn-lg"></button>
	</div>
	<br>
</div>
<br>
<div class="calendar_3_week">
	<div class="col-md-1">
		<button type="button" id="c_3_1" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_3_2" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_3_3" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_3_4" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_3_5" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_3_6" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_3_7" class="btn btn-default btn-lg"></button>
	</div>
	<br>
</div>
<br>
<div class="calendar_4_week">
	<div class="col-md-1">
		<button type="button" id="c_4_1" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_4_2" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_4_3" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_4_4" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_4_5" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_4_6" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_4_7" class="btn btn-default btn-lg"></button>
	</div>
	<br>
</div>
<br>
<div class="calendar_5_week">
	<div class="col-md-1">
		<button type="button" id="c_5_1" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_5_2" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_5_3" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_5_4" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_5_5" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_5_6" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_5_7" class="btn btn-default btn-lg"></button>
	</div>
	<br>
</div>
<br>
<div class="calendar_6_week">
	<div class="col-md-1">
		<button type="button" id="c_6_1" class="btn btn-default btn-lg"></button>
	</div>
	<div class="col-md-1">
		<button type="button" id="c_6_2" class="btn btn-default btn-lg"></button>
	</div>
	<br>
</div>
<form id="showDetailsForm" action="calendar_detail.php" method="POST">
	<input type="hidden" name="year" id="year">
	<input type="hidden" name="month" id="month">
	<input type="hidden" name="date" id="date">
	<input type="hidden" name="token" id="token" value=<?php session_start(); echo $_SESSION['token'] ?> >
</form>
<br>
<script type="text/javascript">
var curr_date = new Date();
var super_year = curr_date.getFullYear();
var super_month = curr_date.getMonth()+1;

function get_the_first_day_now(){
	var date = new Date();
	var y = parseInt(date.getYear()) - 100 + 2000;
	var m = date.getMonth();
	var first_date = new Date(y,m,1);
	var first_day = first_date.getDay();
	return first_day;
}

function get_the_first_day(year,month){
	var y = year;
	var m = month;
	var first_date = new Date(y,m-1,1);
	var first_day = first_date.getDay();
	//console.log(y,m,first_date,first_day);
	return first_day;
}
function get_day_label(week, day){
	var first_day_label = "c_" + week + "_"+day;
	return first_day_label;
}
function get_date_label_month(year, month, day){
	var date_label = "m_" +year +"_"+ month + "_"+day;
	return date_label;
}

function is_over_31(month){
	if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 9 || month == 11){
		return true;
	}
	else {
		return false;
	}
}
function find_year_month_day(str){
	var myRegex = /(\d{4})_(\d{2}|\d)_(\d{2}|\d)/g;
	match=myRegex.exec(str);

	return match;
}
function clicked_date(year, month){
	var maxNum = 32;
	if (!is_over_31(month)){
		maxNum = 31;
	}
	if (is_over_31(month)){
		for (var i = 1; i < maxNum; i++){
			document.querySelector('[md=\"'+get_date_label_month(year,month,i)+"\"]").addEventListener("click", function(evt) {
				var mformatstring = evt['target']['attributes']['md']['nodeValue'];
				var regexMatch = find_year_month_day(mformatstring);
				console.log(regexMatch);
				// year
				var y = regexMatch[1];
				var m = regexMatch[2];
				var d = regexMatch[3];
				document.forms['showDetailsForm'][0].value = y;
				document.forms['showDetailsForm'][1].value = m;
				document.forms['showDetailsForm'][2].value = d;
				// month
				// day
				document.forms['showDetailsForm'].submit();
			},false);
		}
	}
}

function write_dates(year, month){
	var monthNames = ["January", "February", "March", "April", "May", "June",
	  "July", "August", "September", "October", "November", "December"
	];

	document.getElementById("month_text").innerText = monthNames[parseInt(month)-1] + " " + year;

	var max_dates = 32;
	if (is_over_31(month)){
		max_dates = 33;
	}
	var week_count = 1;
	var day_count = 1;
	for (var i = 1; i < 6; i++){
		for (var j = 1; j < 8; j++){
			document.getElementById(get_day_label(i,j)).textContent = "";
		}
		document.getElementById(get_day_label(6,1)).textContent = "";
		document.getElementById(get_day_label(6,2)).textContent = "";
	}
	for (var i = 1; i < 6; i++){
		if (i == 1){
			var first_day = get_the_first_day(year, month);
			if (first_day == 0){
				first_day = 1;
			}
			for (var j = first_day; j < 8; j++){
				document.getElementById(get_day_label(week_count,j)).textContent = day_count;
				document.getElementById(get_day_label(week_count,j)).setAttribute("md",get_date_label_month(year,month,day_count));
				day_count++;
			}
		} else if (i <= 4) {
			for (var j = 1; j < 8; j++){
				document.getElementById(get_day_label(week_count,j)).textContent = day_count;
				document.getElementById(get_day_label(week_count,j)).setAttribute("md",get_date_label_month(year,month,day_count));
				day_count++;
			}
		} else {
			final_week_days = max_dates-day_count;
			if (final_week_days == 9){
				for (var j = 1; j < 8; j++){
					document.getElementById(get_day_label(week_count,j)).textContent = day_count;
					document.getElementById(get_day_label(week_count,j)).setAttribute("md", get_date_label_month(year,month,day_count));
					day_count++;
				}
				document.getElementById(get_day_label(6,1)).textContent = day_count;
				document.getElementById(get_day_label(6,1)).setAttribute("md", get_date_label_month(year,month,day_count));		
			} else {
				for (var j = 1; j <final_week_days; j++){
					document.getElementById(get_day_label(week_count,j)).textContent = day_count;
					document.getElementById(get_day_label(week_count,j)).setAttribute("md", get_date_label_month(year,month,day_count));
					day_count++;
				}
			}
			
		}
		week_count++;
	}

	clicked_date(year, month);
}
write_dates(super_year,super_month);

document.getElementById("prev_m").addEventListener("click", function(evt) {
	if (super_month == 1){
		super_month = 12;
		super_year--;
	} else {
		super_month--;
	}
	write_dates(super_year,super_month);
}, false);
document.getElementById("next_m").addEventListener("click", function(evt) {
	if (super_month == 12){
		super_month = 1;
		super_year++;
	} else {
		super_month++;
	}

	write_dates(super_year,super_month);
}, false);



</script>
</body>
</html>
