<?php
	if (!isset($_POST['token']) && !isset($_GET['token'])){
		die( "Token not sent!" );
		exit;
	} 
	if (!isset($_POST['token']) ){
		session_id($_GET['token']);
	} else {
		session_id($_POST['token']);
	}
    session_start();

	if($_SESSION['token'] !== $_POST['token']){
		die("Request forgery detected");
	}

	require 'database.php';
	//phpinfo();
	$year = $_POST['year'];
	$month = $_POST['month'];
	$date =  $_POST['date'];
	$user_id = $_SESSION['user_id'];
	date_default_timezone_set("America/Chicago");
?>
<!DOCTYPE html>
<html>
<head><title>Module 5 - Calendar </title>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
</head>
<body>
	<?php
		printf("%s%s%s%s%s%s%s", "<h3><p>", $year,"-",$month,"-",$date,"</h3></p>");
	?>
	<p>
		<form action="event_create.php" name="new_evt" method="post">
			<input type="hidden" name="token" id="token" value=<?php session_start(); echo $_SESSION['token'] ?> >
			<input type="submit" value="NEW EVENT">
		</form>
		<form action="calendar.php" id="backtocal" method="post">
		</form>
	</p>

	<h3><p>List of Events:</p></h3>
	
	<ul>
	<?php
		session_start();
		$user_id = $_SESSION['user_id'];
		$date = date_create();
		date_date_set($date, $year, $month, $_POST['date']);
		ob_start();
		echo date_format($date, 'Y-m-d');
		$date = ob_get_contents();
		ob_end_clean();
		# find the list 
		$stmt = $mysqli->prepare("select id,event_time,description from events where user_id=? and DATE(event_time) = ? ORDER BY event_time ASC");

		if(!$stmt){
	   		 printf("Query Prep Failed: %s\n", $mysqli->error);
	   		 exit;
		}
		if($_SESSION['token'] !== $_POST['token']){
			die("Request forgery detected");
		}

		$stmt->bind_param('ss', $user_id, $date);
		$stmt->execute();
		$stmt->bind_result($event_id, $event_time, $description);
		while ($stmt->fetch()){
			echo "<li>";
			$date = date_create($event_time);
			echo date_format($date,'g:i A');
			echo " ".$description.'<form name="delete" >
				<input type="hidden" id= "event_id_del" name="event_id_del" value="'.$event_id .'" >
				<input type="hidden" id= "token_del" name="token_del" value="'.$_SESSION['token'].'">
				<input type="submit" value="Delete">
			</form>
			<form action="event_edit.php" name="edit" method="post">
				<input type="hidden" id="event_id" name="event_id" value="'.$event_id .'" >
				<input type="hidden" id="token" name="token" value="'.$_SESSION['token'].'">
				<input type="submit" value="Edit">
			</form>
			</li>';
			//echo date_format($date,'g:i A');
		}
		//header("refresh:2; url=calendar.php");
		//echo "An event was created successfully!";

		$stmt->close();
		$mysqli->close();
		
	?>
	</ul>
<?php
echo '<script type="text/javascript">';
echo '$(\'[name="delete"]\').submit(function(e){
	e.preventDefault();
	/* get some values from elements on the page: */
    var $form = $( this ),
    url ="/event_delete.php";
    /* Send the data using post */
    var posting = $.post( url, { event_id: e.target[0].value, token: e.target[1].value } );
    /* Alerts the results */
    posting.always(function( d  ) {
    	if (d.success === "false"){
    		alert("Failed:" + d.message);
    	} else {
    		alert("Successfully deleted an event!");
			location.reload();
    	}
    });
});
</script>
'; ?>

</body>
</html>