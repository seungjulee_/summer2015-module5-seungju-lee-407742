<?php
	header("Content-Type: application/json");
	if (!isset($_POST['token'])){
		$msg = array(
   		    "success" => false,
   		    "message" => "Token not sent!"
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
		exit;
	} 

	session_id($_POST['token']);
    session_start();

	if($_SESSION['token'] !== $_POST['token']){
		$msg = array(
   		    "success" => false,
   		    "message" => "Request forgery detected"
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
		exit;
	}
	require 'database.php';

	$event_id = $_POST["event_id"];
	$user_id = $_SESSION['user_id'];
	
	
	$stmt = $mysqli->prepare("delete from events where events.id = ? and events.user_id=?");
	
	if(!$stmt){
   		$msg = array(
   		    "success" => false,
   		    "message" => "Query Prep Failed: %s\n", $mysqli->error
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
   		
	} else {
		$stmt->bind_param('ii', $event_id, $user_id);
		$stmt->execute();

		$msg = array(
	        "success" => true,
	        "token" => $_SESSION['token']
		);
		die();
		echo json_encode($msg, JSON_PRETTY_PRINT);
	}

	$stmt->close();
	$mysqli->close();
	exit;
?>	