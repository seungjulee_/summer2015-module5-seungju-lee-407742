<?php 
	if (!isset($_POST['token'])){
		$msg = array(
   		    "success" => false,
   		    "message" => "Token not sent!"
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
		exit;
	} 

	session_id($_POST['token']);
  session_start();

	if($_SESSION['token'] !== $_POST['token']){
		$msg = array(
   		    "success" => false,
   		    "message" => "Request forgery detected"
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
		exit;
	}
	require 'database.php';
?>
<!DOCTYPE html>
<html>
<head><title>Module 5 - Calendar </title>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
</head>
<body>
<form name="newevent" id="newevent">
	<h3><p>Enter the event time: </p></h3>
	<input type="datetime-local" name="dtime" id="dtime"> 
	<h3><p>Enter the event description: </p></h3>
	<textarea type="text" id="description" name="description" id="2" cols="30" rows="30" class="form-control input-sm chat-input"></textarea><br>

	<input type="hidden" id="token" name="token" value=<?php session_start(); echo $_SESSION['token'];?>>
	
	<input type= "submit">
	<br>
	<br>
</form>
<script type="text/javascript">
$("#newevent").submit(function(e){
	e.preventDefault();
	/* get some values from elements on the page: */
    var $form = $( this ),
    url = '/post_new_event.php';
    /* Send the data using post */
    var posting = $.post( url, { dtime: $('#dtime').val(), description: $('#description').val(), token:$('#token').val() } );
    /* Alerts the results */
    posting.done(function( d  ) {
    	console.log(d);
    	if (d.success){
    		alert("Successfully created an event!");
    		var url = '/calendar.php';
    		var form = $('<form action="' + url + '" method="post">' +
    		  '<input type="hidden" name="token" id="token" value="' + d.token + '" />' +
    		  '</form>');
    		$('body').append(form);
    		form.submit();
    	} else {
    		alert("Log in Failed: " + d.message);
    	}
    });
    posting.fail(function(d){
    	alert("Log in Failed: " + d.message);
    	console.log(d);
    })
});

</script>
</body>
</html>