<?php
header("Content-Type: application/json");
if (!isset($_POST['token'])){
	$msg = array(
		    "success" => false,
		    "message" => "Token not sent!"
		);
		echo json_encode($msg, JSON_PRETTY_PRINT);
}

session_id($_POST['token']);
session_start();
$_POST['session'] = $_POST['token'];
require 'database.php';

$firstname = $_POST["first"];
$lastname = $_POST["last"];
$username = $_POST["username"];
$email = $_POST['email'];

$password = crypt($_POST['password']);

$stmt = $mysqli->prepare("insert into users (first_name, last_name, username, email_address, password) values (?,?,?,?,?)");

if(!$stmt){
		$msg = array(
		    "success" => false,
		    "message" => "Query Prep Failed: %s\n", $mysqli->error
		);
		echo json_encode($msg, JSON_PRETTY_PRINT);
		
} else {
	$stmt->bind_param('sssss', $firstname, $lastname, $username, $email, $password);
	$stmt->execute();

	$msg = array(
        "success" => true,
        "token" => $_SESSION['token']
	);
	echo json_encode($msg, JSON_PRETTY_PRINT);
}

$stmt->close();
$mysqli->close();
exit;
?>