<?php
	header("Content-Type: application/json");
	if (!isset($_POST['token'])){
		$msg = array(
   		    "success" => false,
   		    "message" => "Token not sent!"
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
	} 

	session_id($_POST['token']);
    session_start();

	if($_SESSION['token'] !== $_POST['token']){
		$msg = array(
   		    "success" => false,
   		    "message" => "Request forgery detected"
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
	}
	require 'database.php';

	$event_id = $_POST["event_id"];
	$dt = $_POST['dtime'];
	$desc = $_POST['description'];
	
	$stmt = $mysqli->prepare("UPDATE events SET events.description=?, events.event_time=? WHERE events.id = ?");

	if(!$stmt){
   		$msg = array(
   		    "success" => false,
   		    "message" => "Query Prep Failed: %s\n", $mysqli->error
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
   		
	} else {
		$stmt->bind_param('sss', $desc, $dt, $event_id);
		$stmt->execute();

		$msg = array(
	        "success" => true,
	        "token" => $_SESSION['token']
		);
		echo json_encode($msg, JSON_PRETTY_PRINT);
	}

	$stmt->close();
	$mysqli->close();
	exit;


?>