<?php 
    $token = substr(md5(rand()), 0, 10);
    session_id($token);
    session_start();
    $_SESSION['token'] = $token;
    require 'database.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<title>Sign-Up</title>
</head>
<body id="body-color">
        <div id="Sign-Up">
        <fieldset style="width:30%"><legend>Registration Form</legend>
        <table border="0">
        <form method="POST" name="register">
        <tr>
                <td>First Name</td><td> <input type="text" id="first"></td>
        </tr>
        <tr>
                <td>Last Name</td><td> <input type="text" id="last"></td>
        </tr>
        <tr>
                <td>Email</td><td> <input type="text" id="email"></td>
        </tr>
        <tr>
                <td>Username</td><td> <input type="text" id="username"></td>
        </tr>
        <tr>
                <td>Password</td><td> <input type="password" id="password"></td>
        </tr>
        <tr>
                <td>Confirm Password </td><td><input type="password" id="cpassword"></td>
        </tr>
        <input type="hidden" id="token" name="token" value=

        <?php session_start(); echo $_SESSION['token'];?>>
        <tr>
                <td><input id="mybutton" name="mybutton" type="submit"  value="save"></td>
        </tr>
        </form>
        </table>
        </fieldset>
        </div>
</body>
<script type="text/javascript">
$('[name="register"]').submit(function(e){
    e.preventDefault();
    /* get some values from elements on the page: */
     url = '/register.php';
    // /* Send the data using post */
    var posting = $.post( url, { first: $('#first').val(), last: $('#last').val(), username:$('#username').val(), email:$('#email').val() , password: $('#password').val(), token:$('#token').val()  });
    //  Alerts the results 
    posting.always(function( d  ) {
        console.log(d);
        if (d.success){
            alert("Successfully registered!");
            window.location = "/index.php";
        } else {
            alert("Registeration failed Failed: " + d.message);
        }
    });
});

</script>
</html>

