<?php
 
require 'database.php';
//
header("Content-Type: application/json");
// Use a prepared statement
$stmt = $mysqli->prepare("SELECT id, password FROM users WHERE username=?");
 
// Bind the parameter
$stmt->bind_param('s', $username);
$username = $_POST['username'];
$pwd_guess = $_POST['password'];
$stmt->execute();
 
// Bind the results
$stmt->bind_result($user_id, $pwd_hash);
$stmt->fetch();

// Compare the submitted password to the actual password hash
if( crypt($pwd_guess, $pwd_hash)==$pwd_hash){
	$previous_name = session_name(substr(md5(rand()), 0, 5));
    $token = substr(md5(rand()), 0, 10);

    session_id($token);
    session_start();
    $_SESSION['user_id'] = $user_id;
    $_SESSION['username'] = $username;
    $_SESSION['token'] = $token;
    
    $msg = array(
        "success" => true,
        "token" => $token
    );
    
} else {
    $msg = array(
        "success" => false,
        "message" => "Incorrect Username or Password"
    ); 
}
echo json_encode($msg, JSON_PRETTY_PRINT);
$mysqli->close();
exit;
?>
