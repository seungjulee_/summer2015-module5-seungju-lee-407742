<!DOCTYPE HTML>
<html>
<head>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<title>Sign-In</title>
</head>
<body id="body-color">
	<div id="Sign-In">
		<fieldset style="width:30%"><legend>LOG-IN HERE</legend>
			<form id="login" method="POST" action="connect2.php">
				Username <br><input type="text" id="username" name="username" size="40"><br>
				Password <br><input type="password" id="password" name="password" size="40"><br>
				<input id="button" type="submit" name="submit" value="Log-In">
			</form>
			<br>
			<form id="register">
				<input type = "submit" value= "Register"/>
			</form>	
		</fieldset>
	</div>
	<script type="text/javascript">
	$("#login").submit(function(e){
		e.preventDefault();
		/* get some values from elements on the page: */
	    var $form = $( this ),
        url = $form.attr( 'action' );
        console.log("username is "+ $('#password').val());
	    /* Send the data using post */
	    var posting = $.post( url, { username: $('#username').val(), password: $('#password').val() } );
	    /* Alerts the results */
	    posting.done(function( d  ) {
	    	console.log(d);
	    	console.log(d.success);
	    	if (d.success){
	    		var url = '/calendar.php';
	    		var form = $('<form action="' + url + '" method="post">' +
	    		  '<input type="hidden" name="token" id="token" value="' + d.token + '" />' +
	    		  '</form>');
	    		$('body').append(form);
	    		form.submit();
	    	} else {
	    		alert("Log in Failed: " + d.message);
	    	}
	    });
	    posting.fail(function(d){
	    	alert("Log in Failed: " + d.message);
	    	console.log(d);
	    })
	});
	$("#register").submit(function(e){
		e.preventDefault();
		window.location = "/register_view.php";
	});
	</script>
</body>

</html> 
