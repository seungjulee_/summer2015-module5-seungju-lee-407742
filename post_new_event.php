<?php
	header("Content-Type: application/json");
	if (!isset($_POST['token'])){
		$msg = array(
   		    "success" => false,
   		    "message" => "Token not sent!"
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
	} 

	session_id($_POST['token']);
    session_start();

	if($_SESSION['token'] !== $_POST['token']){
		$msg = array(
   		    "success" => false,
   		    "message" => "Request forgery detected"
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
	}

	require 'database.php';

	$date = $_POST["dtime"];
	$description = $_POST["description"];
	$user_id = $_SESSION['user_id'];
	
	
	$stmt = $mysqli->prepare("insert into events (user_id, description, event_time) values (?,?,?)");

	if(!$stmt){
   		$msg = array(
   		    "success" => false,
   		    "message" => "Query Prep Failed: %s\n", $mysqli->error
   		);
   		echo json_encode($msg, JSON_PRETTY_PRINT);
   		
	} else {
		$stmt->bind_param('sss', $user_id, $description, $date);
		$stmt->execute();

		$msg = array(
	        "success" => true,
	        "token" => $_SESSION['token']
		);
		echo json_encode($msg, JSON_PRETTY_PRINT);
	}

	$stmt->close();
	$mysqli->close();
	exit;
	
?>